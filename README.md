# Webarchitects Website

The SHTML files for [the Webarchitects website](https://www.webarchitects.coop/).

The templates used are in the [webarch-templates](https://git.coop/webarch/website-templates) repo. 

The XML sitemap is generated using [this sitemap generator](https://github.com/webarch-coop/sitemap-generator) and it
is styled using [this sitemap stylesheet](https://github.com/webarch-coop/sitemap-stylesheet).
