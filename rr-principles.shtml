<!--#set var="PAGE_TITLE" 

  value="Radical Routes" 

--><!--#set var="PAGE_DESC" 

  value="Webarchitects has endorsed the Radical Routes Aims and Principles and is an Associate Member of the Radical Routes Co-operative Network." 

--><!--#set var="PAGE_KEYWDS" 

  value="" 

--><!--#set var="PAGE_IMG" 

  value="" 

--><!--#set var="PAGE_SECTION" 

  value="about" 

--><!--#include virtual="/wsh/top.shtml" -->

<p>The Webarchitects 2014 AGM agreed the
<a href="http://radicalroutes.org.uk/aims-and-principles.html">Radical Routes Aims and Principles</a>
and in November 2014 our Associate Membership of the 
<a href="http://radicalroutes.org.uk/">Radical Routes network of co-ops</a> was approved.</p>

<h2 id="aims">Aims and Principles <a href="#aims" class="a">#</a></h2>

<p>Here we are in twenty-first-century Britain, in a world not of our making
but one that has been moulded over thousands of years of exploitation and
injustice.</p>

<p>Our world is shaped by the forces of greed, capitalism and materialism,
where maximum production and optimum profits are vigorously pursued, making
life a misery for many and putting us and the environment at risk.</p>

<p>The system is ultimately controlled by the rich and powerful, the
capitalists and bureaucrats, through the use of many mechanisms such as
ownership of the economy (making people slaves to a job) and control of the
media (creating a passive culture).</p>

<p>Radical Routes is a network of co-ops and individuals seeking to change all
this.</p>

<p>We want to see a world based on equality and co-operation, where people give
according to their ability and receive according to their needs, where work is
fullfilling and useful and creativity is encouraged, where decision making is
open to everyone with no hierarchies, where the environment is valued and
respected in its own right rather than exploited.</p>

<p>We want to take control over all aspects of our lives. However, as we are
not all in a position of control we are forced to compromise in order to
exist.</p>

<p>We are working towards taking control over our housing, education and work
through setting up housing and worker co-ops, and co-operating as a
network.</p>

<p>Through gaining collective control over these areas we aim to reduce
reliance on exploitative structures and build secure bases from which to
challenge the system and encourage others to do so.</p>

<p>Radical Routes has developed from an idea in the mid 80s to a secondary
co-op registered in April 1992. In 1986 New Education Housing Co-op was loaned
&pound;7,000 by supporters to put down a deposit on a house in Birmingham.</p>

<p>From that housing co-op Radical Routes has grown via staging educational
events, the spreading of information, a will to seek likeminded people and
investment from supporters into a expanding nationwide network of like-minded
co-ops and individuals.</p>

<p>The formation of the secondary co-op provided a structure to pursue our
collective aims in a more efficient way, by promoting and raising funds for its
member co-ops.</p>

<p>Radical Routes has limited resources and recognises that its particular work
towards the above aims are only some of many valid activities.</p>

<p>The specific means it is pursuing are:</p>

<ul>
<li>The setting up of housing co-ops to house people and projects with the above aims.</li>
<li>The setting up of workers co-ops which operate with the above aims.</li>
<li>The promotion and organisation of participatory education through skills- and knowledge-sharing events, Taking Control events, informative material and workshops.</li>
<li>The raising of finance to take control over resources (property, technology, land...) through co-operation and economic interlocking of the co-ops.</li>
<li>The support of like-minded projects.</li>
</ul>

<p>Last Updated on Tuesday, 11 June 2013 12:07</p>

<!--#include virtual="/wsh/bot.shtml" -->

